<?php

namespace App\Entity;

use App\Repository\ClientsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientsRepository", repositoryClass=ClientsRepository::class)
 */
class Clients
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $lastname;


    /**
     * @ORM\Column(type="string", length=200)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telephone;

    /**
     * @ORM\Column(type="date")
     */
    private $eraseDataDay;

    /**
     * @ORM\Column(type="boolean")
     */
    private $dataRetentionConsent;

    /**
     * @ORM\ManyToOne(targetEntity=Addresses::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $address_id;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }


    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEraseDataDay(): ?\DateTimeInterface
    {
        return $this->eraseDataDay;
    }

    public function setEraseDataDay(\DateTimeInterface $eraseDataDay): self
    {
        $this->eraseDataDay = $eraseDataDay;

        return $this;
    }

    public function isDataRetentionConsent(): ?bool
    {
        return $this->dataRetentionConsent;
    }

    public function setDataRetentionConsent(bool $dataRetentionConsent): self
    {
        $this->dataRetentionConsent = $dataRetentionConsent;

        return $this;
    }

    public function getAddressId(): ?Addresses
    {
        return $this->address_id;
    }

    public function setAddressId(Addresses $address_id): self
    {
        $this->address_id = $address_id;

        return $this;
    }
}
