<?php

namespace App\Entity;

use App\Repository\RentalsPhotosRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RentalsPhotosRepository::class)
 */
class RentalsPhotos
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $img;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class, inversedBy="photos")
     */
    private $product_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentalId(): ?int
    {
        return $this->rental_id;
    }

    public function setRentalId(int $rental_id): self
    {
        $this->rental_id = $rental_id;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getProductId(): ?Products
    {
        return $this->product_id;
    }

    public function setProductId(?Products $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }
}
