<?php

namespace App\Entity;

use App\Repository\ProductsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductsRepository::class)
 */
class Products
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Disponibilites::class, mappedBy="product_id", cascade={"persist", "remove"})
     */
    private $product_dispo;

    /**
     * @ORM\OneToMany(targetEntity=Bookings::class, mappedBy="product_id")
     */
    private $bookings;

    /**
     * @ORM\OneToMany(targetEntity=OwnersContracts::class, mappedBy="product_id")
     */
    private $ownersContracts;

    /**
     * @ORM\ManyToOne(targetEntity=RentalsTypes::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $rental_type;

    /**
     * @ORM\ManyToOne(targetEntity=Owners::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner_id;

    /**
     * @ORM\OneToMany(targetEntity=RentalsPhotos::class, mappedBy="product_id")
     */
    private $photos;


    public function __construct()
    {
        $this->bookings = new ArrayCollection();
        $this->ownersContracts = new ArrayCollection();
        $this->photos = new ArrayCollection();
        $this->disponibilites = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentalType(): ?RentalsTypes
    {
        return $this->rental_type;
    }

    public function setRentalType(RentalsTypes $rental_type): self
    {
        $this->rental_type = $rental_type;

        return $this;
    }

    public function getOwnerId(): ?Owners
    {
        return $this->owner_id;
    }

    public function setOwnerId(Owners $owner_id): self
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProductDispo(): ?Disponibilites
    {
        return $this->product_dispo;
    }

    public function setProductDispo(Disponibilites $product_dispo): self
    {
        // set the owning side of the relation if necessary
        if ($product_dispo->getProductId() !== $this) {
            $product_dispo->setProductId($this);
        }

        $this->product_dispo = $product_dispo;

        return $this;
    }

    /**
     * @return Collection<int, Bookings>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Bookings $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setProductId($this);
        }

        return $this;
    }

    public function removeBooking(Bookings $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getProductId() === $this) {
                $booking->setProductId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, OwnersContracts>
     */
    public function getOwnersContracts(): Collection
    {
        return $this->ownersContracts;
    }

    public function addOwnersContract(OwnersContracts $ownersContract): self
    {
        if (!$this->ownersContracts->contains($ownersContract)) {
            $this->ownersContracts[] = $ownersContract;
            $ownersContract->setProductId($this);
        }

        return $this;
    }

    public function removeOwnersContract(OwnersContracts $ownersContract): self
    {
        if ($this->ownersContracts->removeElement($ownersContract)) {
            // set the owning side to null (unless already changed)
            if ($ownersContract->getProductId() === $this) {
                $ownersContract->setProductId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RentalsPhotos>
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(RentalsPhotos $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->setProductId($this);
        }

        return $this;
    }

    public function removePhoto(RentalsPhotos $photo): self
    {
        if ($this->photos->removeElement($photo)) {
            // set the owning side to null (unless already changed)
            if ($photo->getProductId() === $this) {
                $photo->setProductId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Disponibilites>
     */
    public function getDisponibilites(): Collection
    {
        return $this->disponibilites;
    }

    public function addDisponibilite(Disponibilites $disponibilite): self
    {
        if (!$this->disponibilites->contains($disponibilite)) {
            $this->disponibilites[] = $disponibilite;
            $disponibilite->setProductId($this);
        }

        return $this;
    }

    public function removeDisponibilite(Disponibilites $disponibilite): self
    {
        if ($this->disponibilites->removeElement($disponibilite)) {
            // set the owning side to null (unless already changed)
            if ($disponibilite->getProductId() === $this) {
                $disponibilite->setProductId(null);
            }
        }

        return $this;
    }
}
