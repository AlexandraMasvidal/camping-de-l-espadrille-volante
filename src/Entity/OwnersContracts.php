<?php

namespace App\Entity;

use App\Repository\OwnersContractsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OwnersContractsRepository::class)
 */
class OwnersContracts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="date")
     */
    private $contract_date;

    /**
     * @ORM\ManyToOne(targetEntity=Products::class, inversedBy="ownersContracts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product_id;

    /**
     * @ORM\ManyToOne(targetEntity=Owners::class, inversedBy="ownersContract")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }

    public function setOwnerId(int $owner_id): self
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    public function getRentalId(): ?int
    {
        return $this->rental_id;
    }

    public function setRentalId(int $rental_id): self
    {
        $this->rental_id = $rental_id;

        return $this;
    }

    public function getContractDate(): ?\DateTimeInterface
    {
        return $this->contract_date;
    }

    public function setContractDate(\DateTimeInterface $contract_date): self
    {
        $this->contract_date = $contract_date;

        return $this;
    }

    public function getProductId(): ?Products
    {
        return $this->product_id;
    }

    public function setProductId($product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }
}
