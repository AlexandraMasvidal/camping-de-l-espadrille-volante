<?php

namespace App\Entity;

use App\Repository\AddressesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddressesRepository::class)
 */
class Addresses
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $num;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $road_type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $road_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=110)
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=Owners::class, mappedBy="address_id")
     */
    private $owners;

    public function __construct()
    {
        $this->owners = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(int $num): self
    {
        $this->num = $num;

        return $this;
    }

    public function getRoadType(): ?string
    {
        return $this->road_type;
    }

    public function setRoadType(string $road_type): self
    {
        $this->road_type = $road_type;

        return $this;
    }

    public function getRoadName(): ?string
    {
        return $this->road_name;
    }

    public function setRoadName(string $road_name): self
    {
        $this->road_name = $road_name;

        return $this;
    }

    public function getZip(): ?int
    {
        return $this->zip;
    }

    public function setZip(int $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection<int, Owners>
     */
    public function getOwners(): Collection
    {
        return $this->owners;
    }

    public function addOwner(Owners $owner): self
    {
        if (!$this->owners->contains($owner)) {
            $this->owners[] = $owner;
            $owner->setAddressId($this);
        }

        return $this;
    }

    public function removeOwner(Owners $owner): self
    {
        if ($this->owners->removeElement($owner)) {
            // set the owning side to null (unless already changed)
            if ($owner->getAddressId() === $this) {
                $owner->setAddressId(null);
            }
        }

        return $this;
    }
}
