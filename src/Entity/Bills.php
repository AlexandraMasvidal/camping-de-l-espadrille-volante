<?php

namespace App\Entity;

use App\Repository\BillsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BillsRepository::class)
 */
class Bills
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="integer")
     */
    private $address_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rental_name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $pool_adult_pu;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $pool_kid_pu;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $ts_adult_pu;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $ts_kids_pu;

    /**
     * @ORM\Column(type="integer")
     */
    private $nb_adults;

    /**
     * @ORM\Column(type="integer")
     */
    private $kids_nb;

    /**
     * @ORM\Column(type="date")
     */
    private $check_in;

    /**
     * @ORM\Column(type="date")
     */
    private $check_out;

    /**
     * @ORM\Column(type="integer")
     */
    private $pool_adults_nb;

    /**
     * @ORM\Column(type="integer")
     */
    private $pool_kids_nb;

    /**
     * @ORM\Column(type="integer")
     */
    private $product_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAddressId(): ?int
    {
        return $this->address_id;
    }

    public function setAddressId(int $address_id): self
    {
        $this->address_id = $address_id;

        return $this;
    }

    public function getRentalName(): ?string
    {
        return $this->rental_name;
    }

    public function setRentalName(string $rental_name): self
    {
        $this->rental_name = $rental_name;

        return $this;
    }

    public function getPoolAdultPu(): ?string
    {
        return $this->pool_adult_pu;
    }

    public function setPoolAdultPu(string $pool_adult_pu): self
    {
        $this->pool_adult_pu = $pool_adult_pu;

        return $this;
    }

    public function getPoolKidPu(): ?string
    {
        return $this->pool_kid_pu;
    }

    public function setPoolKidPu(string $pool_kid_pu): self
    {
        $this->pool_kid_pu = $pool_kid_pu;

        return $this;
    }

    public function getTsAdultPu(): ?string
    {
        return $this->ts_adult_pu;
    }

    public function setTsAdultPu(string $ts_adult_pu): self
    {
        $this->ts_adult_pu = $ts_adult_pu;

        return $this;
    }

    public function getTsKidsPu(): ?string
    {
        return $this->ts_kids_pu;
    }

    public function setTsKidsPu(string $ts_kids_pu): self
    {
        $this->ts_kids_pu = $ts_kids_pu;

        return $this;
    }

    public function getNbAdults(): ?int
    {
        return $this->nb_adults;
    }

    public function setNbAdults(int $nb_adults): self
    {
        $this->nb_adults = $nb_adults;

        return $this;
    }

    public function getKidsNb(): ?int
    {
        return $this->kids_nb;
    }

    public function setKidsNb(int $kids_nb): self
    {
        $this->kids_nb = $kids_nb;

        return $this;
    }

    public function getCheckIn(): ?\DateTimeInterface
    {
        return $this->check_in;
    }

    public function setCheckIn(\DateTimeInterface $check_in): self
    {
        $this->check_in = $check_in;

        return $this;
    }

    public function getCheckOut(): ?\DateTimeInterface
    {
        return $this->check_out;
    }

    public function setCheckOut(\DateTimeInterface $check_out): self
    {
        $this->check_out = $check_out;

        return $this;
    }

    public function getPoolAdultsNb(): ?int
    {
        return $this->pool_adults_nb;
    }

    public function setPoolAdultsNb(int $pool_adults_nb): self
    {
        $this->pool_adults_nb = $pool_adults_nb;

        return $this;
    }

    public function getPoolKidsNb(): ?int
    {
        return $this->pool_kids_nb;
    }

    public function setPoolKidsNb(int $pool_kids_nb): self
    {
        $this->pool_kids_nb = $pool_kids_nb;

        return $this;
    }

    public function getProductId(): ?int
    {
        return $this->product_id;
    }

    public function setProductId(int $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }
}
