<?php

namespace App\Entity;

use App\Repository\OwnersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=OwnersRepository::class)
 * @method string getUserIdentifier()
 */
class Owners implements UserInterface, \Serializable, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $dataRetentionConsent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity=OwnersContracts::class, mappedBy="owner_id")
     */
    private $ownersContract;

    /**
     * @ORM\OneToMany(targetEntity=Products::class, mappedBy="owner_id")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity=Addresses::class, inversedBy="owners")
     */
    private $address_id;

    public function __construct()
    {
        $this->ownersContract = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getAddressId(): ?int
    {
        return $this->address_id;
    }

    public function setAddressId(Addresses $address_id): self
    {
        $this->address_id = $address_id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function isDataRetentionConsent(): ?bool
    {
        return $this->dataRetentionConsent;
    }

    public function setDataRetentionConsent(bool $dataRetentionConsent): self
    {
        $this->dataRetentionConsent = $dataRetentionConsent;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * @return Collection<int, OwnersContracts>
     */
    public function getOwnersContract(): Collection
    {
        return $this->ownersContract;
    }

    public function addOwnersContract(OwnersContracts $ownersContract): self
    {
        if (!$this->ownersContract->contains($ownersContract)) {
            $this->ownersContract[] = $ownersContract;
            $ownersContract->setOwnerId($this);
        }

        return $this;
    }

    public function removeOwnersContract(OwnersContracts $ownersContract): self
    {
        if ($this->ownersContract->removeElement($ownersContract)) {
            // set the owning side to null (unless already changed)
            if ($ownersContract->getOwnerId() === $this) {
                $ownersContract->setOwnerId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Products>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Products $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setOwnerId($this);
        }

        return $this;
    }

    public function removeProduct(Products $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getOwnerId() === $this) {
                $product->setOwnerId(null);
            }
        }

        return $this;
    }

    public function serialize()
    {
        return serialize([
            $this->getId(),
            $this->getEmail(),
            $this->getPassword(),
        ]);

    }

    public function unserialize($data)
    {
        list($this->id, $this->email, $this->password) =
            unserialize($data, ['allowed_classes' => false]);

    }

    public function getRoles()
    {
        return [$this->getRole()];
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // Implement eraseCredentials() method.
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
    }
}
