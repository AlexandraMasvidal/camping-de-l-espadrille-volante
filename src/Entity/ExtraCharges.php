<?php

namespace App\Entity;

use App\Repository\ExtraChargesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExtraChargesRepository::class)
 */
class ExtraCharges
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $label;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $amount_adults;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     */
    private $amount_kids;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getAmountAdults(): ?string
    {
        return $this->amount_adults;
    }

    public function setAmountAdults(string $amount_adults): self
    {
        $this->amount_adults = $amount_adults;

        return $this;
    }

    public function getAmountKids(): ?string
    {
        return $this->amount_kids;
    }

    public function setAmountKids(string $amount_kids): self
    {
        $this->amount_kids = $amount_kids;

        return $this;
    }

}
