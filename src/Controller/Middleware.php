<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use \Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Middleware extends AbstractController
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @return Response
     * @Route("/middleware", name="middleware")
     */
    function index(): Response
    {
        $user = $this->security->getUser();
        if($user->getRole() === 'ROLE_ADMIN') {
            $render = $this->redirectToRoute('bookingsList', []);
        }
        else if($user->getRole() === 'ROLE_OWNER') {
            $render = $this->redirectToRoute('bookings_owner', ['id' => $user->getId()]);
        }
        else{
            dump('meh');
            die;
        }

        return $render;
    }
}